/**
 * Created by מרדכי on 12 אוגוסט 2016.
 */

var ffmpeg = require('fluent-ffmpeg');
var logger = require('./logger');
var fs = require('fs');
var q = require('q');

function isMp3(file) {
    return JSON.parse(file.name).fileExtension === '.mp3';
}

function convertToMp3(file) {
    var deferred = q.defer();
    var path = file.path;

    if (isMp3(file)) {
        deferred.resolve(path);
        return deferred.promise;
    }
    
    
    var newPath = path + '.mp3';

    ffmpeg(path)
        .format('mp3')
        .on('end', function () {
            logger.debug('Converted ' + path + ' to mp3');

            fs.unlink(path, function (err) {
                if (err)
                    logger.debug("Failed to remove remnants of old file, Reason: " + err);
            });

            var name = JSON.parse(file.name);
            name.fileExtension = '.mp3';
            file.name = JSON.stringify(name);

            file.path = newPath;

            deferred.resolve(newPath);
        })
        .on('error', function (err) {
            logger.debug("Failed to convert to mp3, Reason: " + err);

            fs.stat(newPath, function (err, stats) {
                if (!err)
                    fs.unlink(newPath, function (err) {
                        if (err)
                            logger.debug("Failed to remove remnants of mp3, Reason: " + err);
                    });
            });

            deferred.resolve(path);
        })
        .save(newPath);

    return deferred.promise;
}

module.exports.convertToMp3 = convertToMp3;
