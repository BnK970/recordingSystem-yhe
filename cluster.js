/**
 * Created by מרדכי on 29 נובמבר 2016.
 */

var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

var old_app = {};

const OLD_APP = 'old_app';
const APP = 'app';

cluster.on('exit', function (worker, code, signal) {
    console.log('Worker %d died with code/signal %s. Restarting worker...', worker.process.pid, signal || code);
    
    if (worker.process.pid === old_app.process.pid)
        old_app = fork_with_role(OLD_APP);
    else
        fork_with_role(APP);
});

if (cluster.isMaster) {
    for (var i=0; i<numCPUs-1; i++) {
        fork_with_role(APP);
    }
    
    old_app = fork_with_role(OLD_APP);

} else {
    if (process.env['role'] === APP)
        require('./app').execute();
    else if (process.env['role'] === OLD_APP)
        require('./old_app').execute();
}

function fork_with_role(role) {
    var arg = {};
    arg['role'] = role;
    return cluster.fork(arg);
}

