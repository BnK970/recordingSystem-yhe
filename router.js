/**
 * Created by מרדכי on 09 אוגוסט 2016.
 */

var config = require('./config');
var treeScanner = require('./treeScanner');
var logger = require('./modules/logger');
var fileHandler = require('./modules/fileHandler');
var express = require('express');
var router = express.Router();
var q = require('q');
var fs = require('fs');

router.get('/', function (req, res) {
    res.redirect('/index.html');
});

router.post('/api/uploadFile', function (req, res) {
    q.fcall(fileHandler.handleFile, req)
    .then(function (filePath) {
        logger.info('New record arrived. File name: ' + filePath);
        res.sendStatus(200);
    })
    .fail(function (error) {
        logger.error('Error while receiving a record. Error message: ' + error.message);
        res.sendStatus(500);
    })
    .done();
});

router.get('/jstreedata', function (req, res) {
	fs.readFile(__dirname + '/' + config.dirScanner.outFile, 'utf8', function (err, data) {
		if (err) {
			logger.error('Error while reading folder structure. Error message: ' + err.message);
			res.sendStatus(500)
		}
		else {
			jsonString = data;
			jsonString = jsonString.replace(/^(\uFEFF|\uEFBBBF)/, '')  // http://stackoverflow.com/a/24376813
			res.json(JSON.parse(jsonString))
		}
	})
			
})
router.get('/api/refreshFolderList', function (req, res) {
	tree = treeScanner.getTree()
	fs.writeFile(__dirname + '/' + config.dirScanner.outFile,
				 JSON.stringify(tree),
				 'utf8',
				 (err) => {
					if (err) {
						logger.error('Error while updating the tree. Error message: ' + err.message);
					}
					logger.info('Dir tree successfully updated')
				 })
	 res.sendStatus(200)
})

router.get('/infoLog.log', function (req, res) {
    res.sendFile('/var/www/recordingSystem-yhe/logs/infoLog.log')
});

router.get('/debug', function (req, res) {
    res.sendFile('/var/www/recordingSystem-yhe/logs/debugLog.log')
});

module.exports = router;