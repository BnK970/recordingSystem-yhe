/**
 * Created by מרדכי on 30 יוני 2016.
 */

var configuration = {
    server: {
        old_port: 8000,
        new_port: 80
    },
    uploadsFolder: '\\..\\5778',
    logger: {
        infoLogFileName: 'logs/infoLog.log',
        debugLogFileName: 'logs/debugLog.log'
    },
	dirScanner: {
		outFile: 'public/jstreedata.js',
		showTopLevel: false
	},
    metadata: {
        applyModule: true,
        minimumTrackNumber: 1,
        currentYear: '5778'
    },
    uploadToAmazon: {
        applyModule: true,
        accessKeyId: '***',
        secretAccessKey: '***',
        region: 'eu-central-1',
        prefix: '5778/'
    }
};

module.exports = configuration;